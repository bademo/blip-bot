/*

    Blip-Bot
    Copyright (C) 2018  bademo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

// useful variables (change Prefix to change the prefix)
const token = process.env.token || require("./auth.json").token;
const restart = require("./auth.json").restartServer;
const serverApi = require("./auth.json").serverApi;
// if you're hosting on your own server feel free to change token to whatever token you want


let rolemsgID;

const discord = require('discord.js');
const Prefix = "!";
// initialize cooldowns 
const Cooldown = new Set();
const SCooldown = new Set();
// NOTE DO NOT USE
// edit the below things to change the messages the bot sends (<#454468006056296458> and <#454467960241782785> will display as channel links)
const WelcomeMsg = "Welcome to the RFMP Discord server, please make sure to read <#454468006056296458> and <#454467960241782785>";
// initialize client 
const client = new discord.Client();
const team = require('./teams')
team.init(client)
let running = true;



client.on("messageReactionAdd",(msgreaction,user)=>{
    if(rolemsgID == null || msgreaction.message.id !== rolemsgID) return;

    if(msgreaction.emoji.identifier === '1%E2%83%A3'){
        msgreaction.message.guild.member(user).addRole(msgreaction.message.guild.roles.get('569589636880531504'))
    }else if(msgreaction.emoji.identifier === '2%E2%83%A3'){
        msgreaction.message.guild.member(user).addRole(msgreaction.message.guild.roles.get('569591003804336149'))
    }else if(msgreaction.emoji.identifier=== '3%E2%83%A3'){
        msgreaction.message.guild.member(user).addRole(msgreaction.message.guild.roles.get('569590250393960454'))
    }
})
client.on("messageReactionRemove",(msgreaction,user)=>{
    if(rolemsgID == null || msgreaction.message.id !== rolemsgID) return;
    if(msgreaction.emoji.identifier === '1%E2%83%A3'){
        msgreaction.message.guild.member(user).removeRole(msgreaction.message.guild.roles.get('569589636880531504'))
    }else if(msgreaction.emoji.identifier === '2%E2%83%A3'){
        msgreaction.message.guild.member(user).removeRole(msgreaction.message.guild.roles.get('569591003804336149'))
    }else if(msgreaction.emoji.identifier=== '3%E2%83%A3'){
        msgreaction.message.guild.member(user).removeRole(msgreaction.message.guild.roles.get('569590250393960454'))
    }
})


function CheckServers() {
    console.log("ping")
    setTimeout(CheckServers, 60000);
    httpGetAsync(serverApi, callback);

    function callback(body) {
        var servers = body;
        var emb =
        {
            embed: {
                title: `Servers Online: (version: ${servers.version})`,
                description: servers.amount,
                thumbnail: {
                    url: "https://cdn.discordapp.com/avatars/497490017506164736/471acb1d3c7e2f6e7569b918e032f76b.png?size=1024"
                },
                color: 1698076,
                fields: []
            }
        }

        //max 25 fields, probably want to filter least populated ones, or password protected ones
        let amount = (servers.servers.length > 25) ? 25 : servers.servers.length;
        for (let index = 0; index < amount; index++) {
            let pass = "";
            if (servers.servers[index].password == "Password protected")
                pass = ":lock: "

            var f = {
                name: pass + `**${servers.servers[index].name}**`,
                value: `${servers.servers[index].players}/${servers.servers[index].maxPlayers} Players`
            }

            emb.embed.fields.push(f);
        }
        let msId = "569694394622410753";
        let chanId = "569694145426489344";

        let channel = client.channels.get(chanId);
        channel.fetchMessage(msId)
            .then(message => message.edit(emb))
    }
}

// runs when a message is sent
client.on('message', message => {
    if (message.author.bot) {
        //! stops bot from being able to spam it
        return;
    }


    if (message.guild == null) {
        //! stop dm's from breaking it
        return;
    }

    if (message.content.toLowerCase().startsWith(Prefix + "stop") && message.guild.id === "453599228082651137" && (message.guild.member(message.author).hasPermission('KICK_MEMBERS') || message.author.id === "446770194740674560")) {
        //! in case there's a bug, but it hasn't crashed

        console.log(message.author.tag + " toggled stop from " + running);
        running = !running;
    }

    if (message.content.toLowerCase().startsWith(Prefix + "log")) {
        message.reply(`running on os ${process.platform}\nrunning on computer ${require('os').hostname()}`)
    }

    if (message.content.toLowerCase().startsWith(Prefix + "restart") && (message.author.id == '446770194740674560' || message.author.id == '258720417005436929' || message.author.id == '151039550234296320')) {
        httpGetAsync("http://autoupdater.serveo.net/preset?key=emo&cmd=pullblipbot",data=>{
            message.reply(`got ${JSON.stringify(data)} back, restarting!`)
            process.exit()
        },require('http'))

    }


    if (!running) {
        //! part of !stop
        return;
    }


    // !suggest command
    if (message.content.toLowerCase().startsWith(Prefix + "suggest")) {
        if (SCooldown.has(message.author.id)) {
            //! checks cooldown
            return;
        }

        let args = message.content.substring(Prefix.length + "suggest ".length); // get's the suggestion


        if (args === "") {
            //! if no suggestion do nothing
            return;
        }


        client.channels.get("499364414861082625").send(message.author.tag + " suggests: " + args).then(sentmsg => {
            //! sends suggestion and then reacts with a thumbs up and thumbs down
            sentmsg.react("👍").then(() => {
                //! reacts to message with :thumsup: and :thumbsdown:
                sentmsg.react("👎");
            })

            message.channel.send("sent your suggestion succesfully!"); // shows that it sent the suggestion
        });

        client.channels.get("501527005351772171").send(message.author.tag + " suggests: " + args) // nothing here

        SCooldown["add"](message.author.id); // handles cooldown

        setTimeout(() => {
            SCooldown["delete"](message.author.id);
        }, 30000);
    }

   
    //!funhouserestart
    if (message.content.toLowerCase().startsWith(Prefix + 'funhouserestart')) { //restarts cripple rick's server
        //! !funhouserestart command
        let author = message.guild.member(message.author)

        if (author.roles.find('id', '560600488123564047')) {
            httpGetAsync(restart, callback);

            function callback(body) {
                if (body.error === null) {
                    message.channel.send(`**${message.author.username}** Restarted the server!`)
                }
                else {
                    message.channel.send(`Something went wrong! got error ${body.error}`)
                }
            }
        }
        else {
            message.reply("You cannot restart the server!");
        }


    }
       
});


client.on('ready', () => {
    //! on ready function
    console.log("blippo is online!");
    CheckServers();
});

client.on('guildMemberAdd', member => {
    //! on join sends them the contents of WelcomeMsg
    member.send(WelcomeMsg).catch(err => {
        console.log("attempted to send a message to an unkown user")
    });
});

function addCooldown(id) {
    //! adds a cooldown
    Cooldown["add"](id);

    setTimeout(() => {
        Cooldown["delete"](id);
    }, 6000);
}

client.login(token).catch(err => {
    //! login to the client
    console.error("failed to login!")
})

process.title = "Blip Bot host process"



function httpGetAsync(url, callback,method) {
    const https = method || require('https') ;

    https.get(url, (resp) => {
        let data = '';

        resp.on('data', (chunk) => {
            data += chunk;
        });

        resp.on('end', () => {
            callback(JSON.parse(data))
        });

    }).on("error", (err) => {
        console.log("Error: " + err.message);
    });
}