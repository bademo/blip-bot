# BlipBot

## About

BlipBot is a small bot for the RFMP (Ravenfield Multiplayer Project) discord server. It is built using Discord.js, a Node.js framework built around the Discord API.

## Todo

**NOTE: These will slowly be migrated to issues. Please create all new todos there.**

- [x] Add ToLowerCase() to all args (probably finished).
- [x] Clean up a ton (made a release tool).
- [x] Modularize the !team command .
- [x] Put more things in variables.
- [x] Get rid of /node_modules/.
- [x] Change all vars to consts or lets.
